import express from "express";
import data from "./data.js";


const app = express();
const port = 5000 ;

app.get("/api/products",(request,response)=>{
    response.send(data.products)
});

app.get("/api/products/slug/:slug",(request,response)=>{
    const product = data.products.find(item => item.slug === request.params.slug)
    if(product){
        response.send(product)
    }else{
        response.status(404).send({message:"Product  not found"})
    }
});

app.listen(port, () => console.log(`server running on ${port}`));


